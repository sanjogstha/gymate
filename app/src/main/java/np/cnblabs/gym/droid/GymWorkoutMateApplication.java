package np.cnblabs.gym.droid;

import android.app.Application;

import np.cnblabs.gym.droid.Data.Database;

/**
 * Created by nickstamp on 10/10/2015.
 */
public class GymWorkoutMateApplication extends Application {

    private Database database;

    @Override
    public void onCreate() {
        super.onCreate();

        database = Database.getInstance(getApplicationContext());
    }

    public Database getDatabase() {
        return database;
    }
}
